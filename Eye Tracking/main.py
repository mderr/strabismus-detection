import cv2
import dlib
import numpy
from math import sqrt
from statistics import mean

RELATIVE_PATH_PREDICTOR_DATASET = "Eye Tracking/shape_predictor_68_face_landmarks.dat"

video_capture = cv2.VideoCapture(0)
frontal_face_detector = dlib.get_frontal_face_detector()
shape_predictor = dlib.shape_predictor(RELATIVE_PATH_PREDICTOR_DATASET)

VIDEO_FPS = video_capture.get(cv2.CAP_PROP_FPS)
CAPTURE_PERIOD = 1 / VIDEO_FPS

OCCLUSION_COUNTER_LIST = [0] * int(VIDEO_FPS * 1.5)
EYE_POINTS_RIGHT = [36, 37, 38, 39, 40, 41]
EYE_POINTS_LEFT  = [42, 43, 44, 45, 46, 47]
END_EYE_POSITION_LIST   = []
EYE_FRAME_AREA_LIST     = []
START_EYE_POSITION_LIST = []

COLOR_GREEN = (000, 255, 000)
COLOR_RED   = (000, 000, 255)
COLOR_CYAN  = (255, 255, 000)

SIGMA_COLOR = 100
SIGMA_SPACE = 100
PIXEL_NEIGHBORHOOD_DIAMETER = 5
THRESHOLD_VALUE_MINIMUM = 5
THRESHOLD_VALUE_MAXIMUM = 100
THRESHOLD_VALUE_STEP_SIZE = 5
ERODE_ITERATIONS = 5

TEST_EYE_POSITION_COUNTER  = 0
FRAME_COUNTER = 0
START_EYE_POSITION_COUNTER = 0

EYE_TEST_DURATION = 4
FIND_EYE_START_DURATION = 3

START_EYE_POSITION = (None, None)
END_EYE_POSITION   = (None, None)

TARGET_SCLERA_SIZE = 0.55
START_EYE_MOVEMENT_LIMIT = 0.3
IRIS_OCCLUSION_RATIO_THRESHOLD = 0.70
IRIS_SIZE_DIFFERENCE_THRESHOLD = 0.3
MINIMUM_FACE_TO_FRAME_RATIO = 0.4

END_EYE_POSITION_FOUND = False
REMAINING_EYE_IS_OCLUDED = False
TEST_CONCLUDED = False
START_EYE_POSITION_FOUND = False

def draw_crosshair( frame, point_position, color, lenght=4, thickness=1 ):
    position_x, position_y = point_position

    cv2.line( frame, (position_x - lenght, position_y), (position_x + lenght, position_y), color, thickness )
    cv2.line( frame, (position_x, position_y - lenght), (position_x, position_y + lenght), color, thickness )

def get_eye_region( landmarks, eye_points ):
    landmark_point_coordinates = [ (landmarks.part(point).x, landmarks.part(point).y) for point in range(eye_points[0], eye_points[-1] + 1) ]

    return numpy.array(landmark_point_coordinates, numpy.int32)

def get_eye_frame_position( eye_region ):
    global EYE_FRAME_AREA_LIST

    min_x = numpy.min(eye_region[:, 0])
    min_y = numpy.min(eye_region[:, 1])
    max_x = numpy.max(eye_region[:, 0])
    max_y = numpy.max(eye_region[:, 1])

    EYE_FRAME_AREA_LIST.append( (max_x - min_x) * (max_y - min_y) )
    
    return (min_x, min_y), (max_x, max_y)

def get_masked_frame( original_frame, mask_region ):
    frame_resolution_x, frame_resolution_y = original_frame.shape

    empty_frame  = numpy.ones( (frame_resolution_x, frame_resolution_y), numpy.uint8 )
    masked_frame = cv2.fillPoly(empty_frame, [mask_region], 255)

    return cv2.bitwise_and(original_frame, original_frame, mask=masked_frame)

def get_iris_size( iris_frame ):
    frame_resolution_x, frame_resolution_y = iris_frame.shape[:2]
    frame_area = frame_resolution_x * frame_resolution_y
    iris_area  = frame_area - cv2.countNonZero(iris_frame)

    return (iris_area / frame_area) if frame_area > 0 else 0

def get_threshold_frame( original_frame, threshold ):
    kernel_element = numpy.ones( (3, 3), numpy.uint8 )
    filtered_frame = cv2.bilateralFilter(original_frame, PIXEL_NEIGHBORHOOD_DIAMETER, SIGMA_COLOR, SIGMA_SPACE)
    eroded_frame   = cv2.erode(filtered_frame, kernel_element, iterations=ERODE_ITERATIONS)

    _, threshold_frame = cv2.threshold(eroded_frame, threshold, 255, cv2.THRESH_BINARY_INV)

    return threshold_frame

def get_best_threshold_value( frame ):
    trials = {}

    for threshold_value in range(THRESHOLD_VALUE_MINIMUM, THRESHOLD_VALUE_MAXIMUM, THRESHOLD_VALUE_STEP_SIZE):
        iris_frame = get_threshold_frame(frame, threshold_value)
        trials[threshold_value] = get_iris_size(iris_frame)

    best_threshold, iris_size = min( trials.items(), key=(lambda iris_size: abs(iris_size[1] - TARGET_SCLERA_SIZE)) )

    return best_threshold, iris_size

def get_eye_center_position( landmarks, eye_points, threshold=None ):
    eye_region = get_eye_region(landmarks, eye_points)
    (min_x, min_y), (max_x, max_y) = get_eye_frame_position(eye_region)
    masked_eye_frame = get_masked_frame(grayscale_frame, eye_region)
    eye_frame = masked_eye_frame[min_y:max_y, min_x:max_x]

    if threshold is None:
        threshold_value, iris_size = get_best_threshold_value(eye_frame)

    else:
        threshold_value = threshold
        iris_size = get_iris_size( get_threshold_frame(eye_frame, threshold_value) )

    threshold_frame = get_threshold_frame(eye_frame, threshold_value)
    contours, _ = cv2.findContours( threshold_frame, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE, offset=(min_x, min_y) )

    if len(contours) > 0:
        largest_contour = max(contours, key=cv2.contourArea)
        moments = cv2.moments(largest_contour)

        if moments["m00"] != 0:
            contour_center_x = int(moments["m10"] / moments["m00"])
            contour_center_y = int(moments["m01"] / moments["m00"])

            return (contour_center_x, contour_center_y), threshold_value, iris_size, (min_x, min_y)

    return (None, None), threshold_value, iris_size, (min_x, min_y)

def append_to_occlusion_counter( value ):
    global OCCLUSION_COUNTER_LIST

    OCCLUSION_COUNTER_LIST = OCCLUSION_COUNTER_LIST[1:]
    OCCLUSION_COUNTER_LIST.append(value)

def get_movement_percentage( start_coordinates, end_coordinates, reference_frame ):
    start_x, start_y = start_coordinates
    end_x, end_y     = end_coordinates

    distance_moved = sqrt( pow(end_x - start_x, 2) + pow(end_y - start_y, 2) )

    return (distance_moved / reference_frame) * 100

def reset_test():
    global EYE_FRAME_AREA_LIST, START_EYE_POSITION_LIST, START_EYE_POSITION_COUNTER

    EYE_FRAME_AREA_LIST.clear()
    START_EYE_POSITION_LIST.clear()
    START_EYE_POSITION_COUNTER = 0

def save_average_start_position( frame, landmarks, eye_points, capture_period, draw_position=False ):
    global START_EYE_POSITION_COUNTER, START_EYE_POSITION_FOUND, START_EYE_POSITION_LIST, START_EYE_POSITION

    if START_EYE_POSITION_COUNTER * capture_period > FIND_EYE_START_DURATION:
        mean_x = int(mean( [x for x, _ in START_EYE_POSITION_LIST] ))
        mean_y = int(mean( [y for _, y in START_EYE_POSITION_LIST] ))

        START_EYE_POSITION = (mean_x, mean_y)
        START_EYE_POSITION_FOUND = True

    subject_eye_center, _, _, eye_frame_position = get_eye_center_position(landmarks, eye_points)

    if subject_eye_center[0] is not None and subject_eye_center[1] is not None:
        if draw_position:
            draw_crosshair(frame, subject_eye_center, COLOR_GREEN)

        relative_eye_coordinates = tuple( map(lambda i, j: i - j, subject_eye_center, eye_frame_position) )

        if len(START_EYE_POSITION_LIST) > 0:
            eye_movement_percentage = get_movement_percentage(START_EYE_POSITION_LIST[-1], relative_eye_coordinates, mean(EYE_FRAME_AREA_LIST))

            if (eye_movement_percentage > START_EYE_MOVEMENT_LIMIT):
                reset_test()

        START_EYE_POSITION_LIST.append(relative_eye_coordinates)
        START_EYE_POSITION_COUNTER += 1
 
def check_if_remaining_eye_is_occluded( frame, landmarks, subject_eye_points, draw_position=False ):
    global REMAINING_EYE_IS_OCLUDED, EYE_FRAME_AREA_LIST

    remaining_eye_points = EYE_POINTS_RIGHT if subject_eye_points == EYE_POINTS_LEFT else EYE_POINTS_LEFT

    subject_eye_position, subject_eye_threshold, subject_eye_iris_size, _ = get_eye_center_position(landmarks, subject_eye_points)
    _, _, remaining_eye_iris_size, _ = get_eye_center_position(landmarks, remaining_eye_points, threshold=subject_eye_threshold)

    if draw_position:
        draw_crosshair(frame, subject_eye_position, COLOR_RED)

    iris_size_difference = abs(subject_eye_iris_size - remaining_eye_iris_size)
    append_to_occlusion_counter( 1 if iris_size_difference > IRIS_SIZE_DIFFERENCE_THRESHOLD else 0 )
    occlusion_ratio = OCCLUSION_COUNTER_LIST.count(1) / len(OCCLUSION_COUNTER_LIST)

    if occlusion_ratio > IRIS_OCCLUSION_RATIO_THRESHOLD:
        REMAINING_EYE_IS_OCLUDED = True
        EYE_FRAME_AREA_LIST.clear()

def save_average_end_coordinates( frame, landmarks, subject_eye_points, capture_period, draw_position=False ):
    global TEST_EYE_POSITION_COUNTER, END_EYE_POSITION_FOUND, END_EYE_POSITION_LIST, END_EYE_POSITION

    if TEST_EYE_POSITION_COUNTER * capture_period > EYE_TEST_DURATION:
        mean_x = int(mean( [x for x, _ in END_EYE_POSITION_LIST] ))
        mean_y = int(mean( [y for _, y in END_EYE_POSITION_LIST] ))

        END_EYE_POSITION = (mean_x, mean_y)
        END_EYE_POSITION_FOUND = True

    subject_eye_center, _, _, eye_frame_position = get_eye_center_position(landmarks, subject_eye_points)

    if subject_eye_center[0] is not None and subject_eye_center[1] is not None:
        if draw_position:
            draw_crosshair(frame, subject_eye_center, COLOR_CYAN)

        relative_eye_coordinates = tuple( map(lambda i, j: i - j, subject_eye_center, eye_frame_position) )

        END_EYE_POSITION_LIST.append(relative_eye_coordinates)
        TEST_EYE_POSITION_COUNTER += 1

def get_face_to_frame_size_ratio( face, frame ):
    frame_resolution_x, frame_resolution_y = frame.shape

    frame_area = frame_resolution_x * frame_resolution_y
    face_area  = ( face.right() - face.left() ) * ( face.bottom() - face.top() )
    
    return face_area / frame_area

while True:
    if TEST_CONCLUDED:
        print("POSS", START_EYE_POSITION, END_EYE_POSITION)
        print(EYE_MOVEMENT_PERCENTAGE)

        if EYE_MOVEMENT_PERCENTAGE > 0.1:
            print("DANGERT")

        break

    _, frame = video_capture.read()
    grayscale_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = frontal_face_detector(grayscale_frame)

    for face in faces:
        if get_face_to_frame_size_ratio(face, grayscale_frame) < MINIMUM_FACE_TO_FRAME_RATIO:
            cv2.rectangle(frame, (face.left(),face.top()), (face.right(),face.bottom()), (0,255,0), 2)
            cv2.putText(frame, "PLEASE GET CLOSER", (10, 100),cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 255, 0), 2)
            break

        landmarks = shape_predictor(grayscale_frame, face)

        if not START_EYE_POSITION_FOUND:
            save_average_start_position(frame, landmarks, EYE_POINTS_LEFT, CAPTURE_PERIOD, draw_position=True)
            break

        if not REMAINING_EYE_IS_OCLUDED:
            check_if_remaining_eye_is_occluded(frame, landmarks, EYE_POINTS_LEFT, draw_position=True)
            break

        if not END_EYE_POSITION_FOUND:
            save_average_end_coordinates(frame, landmarks, EYE_POINTS_LEFT, CAPTURE_PERIOD, draw_position=True)
            break

        EYE_MOVEMENT_PERCENTAGE = get_movement_percentage(START_EYE_POSITION, END_EYE_POSITION, mean(EYE_FRAME_AREA_LIST))
        TEST_CONCLUDED = True

    cv2.imshow("opencv", frame)
    FRAME_COUNTER += 1

    if cv2.waitKey(1) == ord("q"):
        break

video_capture.release()
cv2.destroyAllWindows()